(function() {
    'use strict';
    angular
        .module('SampleApp')
        .factory('HumanConnect', HumanConnect);

    HumanConnect.$inject = ['$window'];

    function HumanConnect($window) {
        if(!$window.HumanConnect){
            // Handle if HumanConnect is not available
            console.log('Unable to load Human Connect');
        }
        return $window.HumanConnect;
    }
})();
