(function () {

'use strict';

angular
    .module('SampleApp', ['ngRoute','ngAnimate'])
    .config([
        '$locationProvider',
        '$routeProvider',
        function($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');
            // routes
            $routeProvider
                .when("/", {
                    templateUrl: "./partials/partial1.html",
                    controller: "MainController"            
                })
                .when("/human-api", {
                    templateUrl: "./partials/humanapi.html",
                    controller: "humanApiController"
                })
                .otherwise({
                    redirectTo: '/'
                });
        }
    ]);

// Load controller
angular
    .module('SampleApp')
    .controller('MainController', [
        '$scope',
        function($scope) {
            $scope.test = "Testing...";
        }
    ]);

}());