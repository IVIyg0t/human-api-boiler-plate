(function(){

'use strict';

angular
    .module('SampleApp')
    .controller('humanApiController', humanApiController);

    humanApiController.$inject = ['$http', '$scope', 'HumanConnect'];

    function humanApiController($http, $scope, HumanConnect) {

        $scope.clientId = '8b2886484c6487485b82b8a862a4a61f49e60d43';
        $scope.individualId = 'abcdefghijklmnopqrtsuvwxyzz';

        $scope.test = "This is a test of the human API Controller";
        $scope.sessionTokenObject = null;

        $scope.openHumanAPIConnect = function() {
            var options = {
                clientUserId: encodeURIComponent($scope.individualId),
                clientId: $scope.clientId,
                publicToken: '',
                finish: function(err, sessionTokenObject){
                    console.log("Human Connect Finished");
                    console.log(sessionTokenObject);
                },
                close: function(){
                    /* (Optional) Called when a user closes the popup without
                        connecting any data sources                        */
                    console.log("Human Connect Closed: ");
                    console.log(options);
                },
                error: function(err){
                    /* (Optional) Called if an error occurs when loading the popup */
                    console.log("Error occurred");
                    console.log(err);
                },
            };
            HumanConnect.open(options);

        };
    }
}());
